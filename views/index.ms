<html>
<head>
<script
			  src="https://code.jquery.com/jquery-3.2.1.min.js"
			  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			  crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
<h1>{{title}}</h1>
<div class="container">
{{#weeks}}
<div id="{{#printOperatorAsId}}week-{{week}}{{/printOperatorAsId}}" class="week-entry">
	<h2>Week of {{week}}</h2>
	Week Modified <input type="checkbox" class="weekOption weekModified" {{#weekAltered}}checked{{/weekAltered}} /></input>
	<table class="table table-bordered week-day-table">
		<tr>
			<th>Delete</th>
			<th>Day</th>
			<th>Delivery Day</th>
			<th>Route</th>
		</tr>
		{{#days}}
		<tr class="dayEntry">
			<td class="dayOption"><input type="checkbox"/></td>
			<td class="dayOption">{{day}}</td>
			<td class="dayOption">
				<select name="{{deliveryDay}}">
					{{{printDeliveryDaySelectorOptions}}}

				</select>
			</td>
			<td class="dayOption"><input type="text" value="{{routeId}}"/></td>
		</tr>
		{{/days}}
	</table>
</div>
{{/weeks}}
<div class="formSubmit">
	<button name="button" id="fullFormButton" disabled=true>Click me</button>
</div>
</div>
<script>
function handleDayOptionChange(evt){
	$(evt.target).closest(".week-entry").children('.weekModified').prop({'checked':true});
	$('#fullFormButton').prop({'disabled':false});
}
$('.dayOption').change(handleDayOptionChange);
$('.dayOption').on('input',handleDayOptionChange);
$(document).ready(function(){
	if(1 <= $('.weekModified').filter(function(){return $(this).prop('checked');}).length){
		$('#fullFormButton').prop({'disabled':false});
	}
});
</script>
</body>
</html>
