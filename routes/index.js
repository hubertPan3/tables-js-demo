var express = require('express');
var router = express.Router();
var dataHelper = require('../dataHelpers/modelGeneration.js');

/* GET home page. */
router.get('/', function(req, res, next) {
	weekPage(req, res, next);
});

function weekPage(req, res, next){
	var model = dataHelper.generateEmptyModel();

	//calculate Week Information
	var curWeekSunday = new Date();
	curWeekSunday.setDate(curWeekSunday.getDate() - curWeekSunday.getDay());
	var lastWeekSunday = new Date();
	lastWeekSunday.setDate(curWeekSunday.getDate() - 7);
	var nextWeekSunday = new Date();
	nextWeekSunday.setDate(curWeekSunday.getDate() + 7);

	var week1 = dataHelper.generateBlankWeek(lastWeekSunday.toDateString());
	week1.days[2].deliveryDay = "TUE";
	week1.days[2].routeId = "bob";

	var week2 = dataHelper.generateBlankWeek(curWeekSunday.toDateString());
	week2.days[3].deliveryDay = "WED";
	week2.days[3].routeId = "bob2";
	week2.days[4].deliveryDay = "THU";
	week2.days[4].routeId = "bob3";

	var week3 = dataHelper.generateBlankWeek(nextWeekSunday.toDateString());
	week3.weekAltered = true;

	model.weeks.push(week1, week2, week3);
	res.render('index', model);
}

module.exports = router;
