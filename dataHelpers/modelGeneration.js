function generateEmptyModel(){
	var model = {
		printDeliveryDaySelectorOptions: function(){
			var optionsHtml = "";
			var options = ["NA", "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
			options.forEach(function(dayOption){
				if(dayOption != this.deliveryDay){
					optionsHtml= optionsHtml + "<option value='" + dayOption + "'>"+ dayOption +"</option>";
				}else {
					optionsHtml= optionsHtml + "<option value='" + dayOption + "' selected>"+ dayOption +"</option>";
				}
			},this);

			return optionsHtml;
		},
		printOperatorAsId: function(){
			return function(text, render){
				var out = render(text);
				out = out.toLowerCase().replace(/[^a-z0-9]+/g,"-");
				return out;
			}
		},
		title: 'Express',
		weeks: []
	};

	return model;
}

function generateBlankWeek(weekLabel){
	var week = {
		'week': weekLabel,
		'weekAltered': false,
		'days': [
		{
			'day': 'SUN',
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"MON",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"TUE",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"WED",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"THU",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"FRI",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		},
		{
			'day':"SAT",
			'deliveryDay': 'NA',
			'routeId': 'NA'
		}
		]
	};
	return week;
}

exports.generateEmptyModel = () => generateEmptyModel();
exports.generateBlankWeek = (weekLabel) => generateBlankWeek(weekLabel);
